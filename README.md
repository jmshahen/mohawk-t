# Description 
This is a companion repository that solves ATRBAC system, written in the Mohawk+T format, by reducing to a NuSMV file
and running it through NuSMV.  

# How To Use
1. Best way to get working with the code is to open it in Eclipse
2. You will need to have Java 1.8 installed and Ant
3. Load it up in eclipse and go to the Ant window (Window -> Show View -> Ant) and click on "Run the Default Target"  
	> If you do not want to use Eclipse, just run "ant build.xml"
4. 


# Workflows


# Contact
Author: Jonathan Shahen <jonathan.shahen@gmail.com>

# Notes
* In markdown if you want to start a new line you must end the line with two or more spaces [BitBucket Issue - The text editor is losing line breaks](https://bitbucket.org/site/master/issue/7396/the-text-editor-is-losing-line-breaks)
* Eclipse cannot do nested lists, but Bitbucket can: [BitBucket Tutorial - Markdown Demo](https://bitbucket.org/tutorials/markdowndemo/overview#markdown-header-links)