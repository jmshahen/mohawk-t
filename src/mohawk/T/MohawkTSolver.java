package mohawk.T;

import java.util.logging.Logger;

public class MohawkTSolver {
    public static final Logger logger = Logger.getLogger("mohawk");

    public static void main(String[] args) {
        MohawkTSolverInstance inst = new MohawkTSolverInstance();

        inst.run(args);
    }

}
