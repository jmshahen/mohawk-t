package mohawk.T.test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import mohawk.T.MohawkTSolverInstance;
import mohawk.global.results.ExecutionResult;

@RunWith(Parameterized.class)
public class MoawkTSolverRegressionTests {
    public MohawkTSolverInstance instance;
    public String logLevel;
    private String _filename;
    private ExecutionResult _expectedResult;

    @Before
    public void init() {
        System.out.println("\n\n---\nCreating new instance of MohawkTSolverInstance");
        logLevel = "verbose";

        instance = new MohawkTSolverInstance();
    }

    @SuppressWarnings("rawtypes")
    @Parameterized.Parameters
    public static Collection primeNumbers() {
        String reachableFolder = "data/regression/reachable";
        String unreachableFolder = "data/regression/unreachable";

        return Arrays.asList(new Object[][]{//
                {reachableFolder + "/reachable-test01.mohawkT", ExecutionResult.GOAL_REACHABLE},//
                {reachableFolder + "/reachable-test02.mohawkT", ExecutionResult.GOAL_REACHABLE},//
                {reachableFolder + "/reachable-test03.mohawkT", ExecutionResult.GOAL_REACHABLE},//
                {reachableFolder + "/reachable-test04.mohawkT", ExecutionResult.GOAL_REACHABLE},//
                {reachableFolder + "/reachable-test05.mohawkT", ExecutionResult.GOAL_REACHABLE},//
                {reachableFolder + "/reachable-test06.mohawkT", ExecutionResult.GOAL_REACHABLE},//
                {unreachableFolder + "/unreachable-test01.mohawkT", ExecutionResult.GOAL_UNREACHABLE},//
                {unreachableFolder + "/unreachable-test02.mohawkT", ExecutionResult.GOAL_UNREACHABLE},//
                {unreachableFolder + "/unreachable-test03.mohawkT", ExecutionResult.GOAL_UNREACHABLE},//
                {unreachableFolder + "/unreachable-test04.mohawkT", ExecutionResult.GOAL_UNREACHABLE},//
        });
    }

    public MoawkTSolverRegressionTests(String filename, ExecutionResult expectedResult) {
        this._filename = filename;
        this._expectedResult = expectedResult;
    }

    @Test
    public void regressionTest() {
        System.out.println("Testing file: " + _filename);

        instance.run(new String[]{"-run", "all", "-loglevel", logLevel, "-logfile", "n", "-input", _filename});
        assertEquals(_expectedResult, instance.results.getLastResult()._result);
    }
}
