package mohawk.T;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;

import org.apache.commons.cli.Options;

public class MohawkTSolverCUI {
    public static final Logger logger = Logger.getLogger("mohawk");
    public static String previousCommandFilename = "MohawkTSolverCUIPreviousCommand.txt";
    public static String previousCmd;

    public static void main(String[] args) {
        MohawkTSolverInstance inst = new MohawkTSolverInstance();
        ArrayList<String> argv = new ArrayList<String>();
        String cmd = "";
        Scanner user_input = new Scanner(System.in);

        Options options = new Options();
        inst.setupOptions(options);
        inst.printHelp(options, 120);

        printCommonCommands();

        System.out.print("Enter Commandline Argument ('!e' to run): ");
        String quotedStr = "";
        StringBuilder fullCommand = new StringBuilder();
        while (true) {
            cmd = user_input.next();
            fullCommand.append(cmd + " ");

            if (quotedStr.isEmpty() && cmd.startsWith("\"")) {
                System.out.println("Starting: " + cmd);
                quotedStr = cmd.substring(1);
                continue;
            }
            if (!quotedStr.isEmpty() && cmd.endsWith("\"")) {
                System.out.println("Ending: " + cmd);
                argv.add(quotedStr + " " + cmd.substring(0, cmd.length() - 1));
                quotedStr = "";
                continue;
            }

            if (!quotedStr.isEmpty()) {
                quotedStr = quotedStr + " " + cmd;
                continue;
            }

            if (cmd.equals("!e")) {
                break;
            }

            if (cmd.equals("!p")) {
                argv.clear();
                argv.addAll(Arrays.asList(previousCmd.split(" ")));
                break;
            }

            argv.add(cmd);
        }
        user_input.close();

        System.out.println("Commands: " + argv);

        FileWriter fw;
        try {
            if (!cmd.equals("!p")) {
                fw = new FileWriter(previousCommandFilename, false);
                fw.write(fullCommand.toString());
                fw.close();
            }
        } catch (IOException e) {
            System.out.println("[ERROR] Unable to write out previous command to: " + previousCommandFilename);
        }

        inst.run(argv.toArray(new String[1]));
    }

    public static void printCommonCommands() {

        System.out.println("\n\n--- Common Commands ---");
        System.out.println(MohawkTSolverOptionString.RUN.c("all") + MohawkTSolverOptionString.PROGRAM.c("nusmv")
                + MohawkTSolverOptionString.MODE.c("smc")
                + MohawkTSolverOptionString.SPECFILE.c("data/regression/reachable/reachable-test01.mohawkT") + "!e");

        System.out.println(MohawkTSolverOptionString.RUN.c("all") + MohawkTSolverOptionString.PROGRAM.c("nusmv")
                + MohawkTSolverOptionString.MODE.c("smc") + MohawkTSolverOptionString.ABSTRACTION_REFINEMENT.c()
                + MohawkTSolverOptionString.DEBUG.c()
                + MohawkTSolverOptionString.SPECFILE.c("data/regression/reachable/reachable-test07.mohawkT") + "!e");

        System.out.println("\n---\n");

        System.out.println(MohawkTSolverOptionString.RUN.c("dia") + MohawkTSolverOptionString.DEBUG.c()
                + MohawkTSolverOptionString.SPECFILE.c("data/regression/reachable/reachable-test02.mohawkT") + "!e");
        System.out.println(MohawkTSolverOptionString.RUN.c("dia") + MohawkTSolverOptionString.DEBUG.c()
                + MohawkTSolverOptionString.SPECFILE.c("data/regression/reachable/reachable-test07.mohawkT") + "!e");
        System.out.println(MohawkTSolverOptionString.RUN.c("dia") + MohawkTSolverOptionString.DEBUG.c()
                + MohawkTSolverOptionString.SPECFILE.c("data/ranise/hos/AGTHos10.mohawkT") + "!e");

        System.out.println("\n---\n");

        System.out.println(MohawkTSolverOptionString.RUN.c("slice") + MohawkTSolverOptionString.DEBUG.c()
                + MohawkTSolverOptionString.SPECFILE.c("data/regression/reachable/reachable-test02.mohawkT") + "!e");
        System.out.println(MohawkTSolverOptionString.RUN.c("slice") + MohawkTSolverOptionString.DEBUG.c()
                + MohawkTSolverOptionString.SPECFILE.c("data/regression/reachable/reachable-test07.mohawkT") + "!e");
        System.out.println(MohawkTSolverOptionString.RUN.c("slice") + MohawkTSolverOptionString.DEBUG.c()
                + MohawkTSolverOptionString.SPECFILE.c("data/ranise/hos/AGTHos10.mohawkT") + "!e");

        System.out.println("\n---\n");

        System.out.println(MohawkTSolverOptionString.RUN.c("dia") + MohawkTSolverOptionString.BULK.c()
                + MohawkTSolverOptionString.SPECFILE.c("data/regression/reachable/") + "!e");
        System.out.println(MohawkTSolverOptionString.RUN.c("dia") + MohawkTSolverOptionString.BULK.c()
                + MohawkTSolverOptionString.SPECFILE.c("data/regression/unreachable/") + "!e");
        System.out.println(MohawkTSolverOptionString.RUN.c("dia") + MohawkTSolverOptionString.BULK.c()
                + MohawkTSolverOptionString.SPECFILE.c("data/ranise/testsuiteb/") + "!e");
        System.out.println(MohawkTSolverOptionString.RUN.c("dia") + MohawkTSolverOptionString.BULK.c()
                + MohawkTSolverOptionString.SPECFILE.c("data/ranise/testsuitec/hos/") + "!e");
        System.out.println(MohawkTSolverOptionString.RUN.c("dia") + MohawkTSolverOptionString.BULK.c()
                + MohawkTSolverOptionString.SPECFILE.c("data/ranise/testsuitec/univ/") + "!e");

        System.out.println("\n---\n");

        System.out.println(MohawkTSolverOptionString.RUN.c("slice") + MohawkTSolverOptionString.BULK.c()
                + MohawkTSolverOptionString.SPECFILE.c("data/regression/reachable/") + "!e");
        System.out.println(MohawkTSolverOptionString.RUN.c("slice") + MohawkTSolverOptionString.BULK.c()
                + MohawkTSolverOptionString.SPECFILE.c("data/regression/unreachable/") + "!e");
        System.out.println(MohawkTSolverOptionString.RUN.c("slice") + MohawkTSolverOptionString.BULK.c()
                + MohawkTSolverOptionString.SPECFILE.c("data/ranise/testsuiteb/") + "!e");
        System.out.println(MohawkTSolverOptionString.RUN.c("slice") + MohawkTSolverOptionString.BULK.c()
                + MohawkTSolverOptionString.SPECFILE.c("data/ranise/testsuitec/hos/") + "!e");
        System.out.println(MohawkTSolverOptionString.RUN.c("slice") + MohawkTSolverOptionString.BULK.c()
                + MohawkTSolverOptionString.SPECFILE.c("data/ranise/testsuitec/univ/") + "!e");

        System.out.println("");
        try {
            BufferedReader bfr = new BufferedReader(new FileReader(previousCommandFilename));
            previousCmd = bfr.readLine();
            bfr.close();
            System.out.println("Previous Command: " + previousCmd);
        } catch (IOException e) {
            System.out.println("[ERROR] Unable to load previous command!");
        }
    }
}
