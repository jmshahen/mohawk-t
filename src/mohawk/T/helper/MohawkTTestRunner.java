package mohawk.T.helper;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Callable;
import java.util.logging.Logger;

import mohawk.converter.to.nusmv.ConvertToNuSMV;
import mohawk.global.FileExtensions;
import mohawk.global.nusmv.NuSMV;
import mohawk.global.optimization.abstractionrefinement.AbstractionRefinment;
import mohawk.global.optimization.boundestimation.BoundEstimation;
import mohawk.global.pieces.MohawkT;
import mohawk.global.pieces.mohawk.NuSMVMode;
import mohawk.global.results.ExecutionResult;
import mohawk.global.timing.MohawkTiming;

public class MohawkTTestRunner implements Callable<ExecutionResult> {
    public final static Logger logger = Logger.getLogger("mohawk");

    public MohawkTiming timing;
    public String _timerKey;

    private NuSMV _nusmv = null;

    public MohawkT _testcase;
    /** Program path to a NuSMV type program that must have the following format:<br/>
     * <b><i>program</i> -bmc -bmc_length 12 <i>file</i></b>
     * <ul>
     * <li><b><i>program</i></b> - the program path (can be absolute or relative)
     * <li><b>-bmc</b> - switch from Symbolic Model Checker to Bounded Model Checker
     * <li><b>-bmc_length 12</b> - sets the bound for the Bounded Model Checker
     * <li><b><i>file</i></b> - the path to the SMV input file (can be absolute or relative)
     * </ul>
    */
    public String _programPath;
    public File _smv_File;
    public File _policyFile;
    /** When equal to BMC, this turns on Bound Estimation and uses Bounded Model Checking instead of
     * Symbolic Model Checking */
    public NuSMVMode _mode;
    /** When TRUE, this turns on Abstraction refinement */
    public boolean _abstractionRefinement = false;
    /** When TRUE, this skips running the Model Checker */
    public boolean _skipRunning = false;
    /** Turns on Debugging Mode. This produces more output files to show the steps involved in computing the
     * reachability result */
    public boolean debug = false;

    public MohawkTTestRunner(MohawkT testcase, String programPath, String policyFile, File smvFile, NuSMVMode mode,
            String timerKey, MohawkTiming timing, boolean abstractionRefinement, boolean skipRunning) {
        _testcase = testcase;
        _programPath = programPath;
        _smv_File = smvFile;
        _policyFile = new File(policyFile);
        _mode = mode;
        _abstractionRefinement = abstractionRefinement;
        _skipRunning = skipRunning;

        this.timing = timing;
        _timerKey = timerKey + "-MTTR:";
    }

    /** Call this function to kill any process that was started by it */
    public void kill() {
        if (_nusmv != null) {
            _nusmv.execProcess.destroy();
        }
        timing.cancelAll(_timerKey);
    }

    @Override
    public ExecutionResult call() throws Exception {
        ExecutionResult result = null;

        if (_abstractionRefinement) {
            result = abstractionRefinementLoop();
        } else {
            result = convertAndRunModelChecker(_testcase, _skipRunning);
        }

        return result;
    }

    /** This function first converts the input policy {@code policy} to SMV format and stores it in the file
     * {@link MohawkTTestRunner#_smv_File} and then passes this to the model checker in
     * {@link MohawkTTestRunner#_programPath}.
     * 
     * @param policy
     * @param skipRunning
     * @return */
    private ExecutionResult convertAndRunModelChecker(MohawkT policy, boolean skipRunning) {
        ExecutionResult result = null;

        if (debug) {
            String mStr = policy.getString("\n\n", true, true);

            String mohawkTFilename = _smv_File.getAbsolutePath();

            if (mohawkTFilename.endsWith(".smv")) {
                mohawkTFilename = mohawkTFilename.substring(0, mohawkTFilename.length() - 4);
            }

            mohawkTFilename = mohawkTFilename + new FileExtensions().Mohawk_T;

            try {
                logger.info("[FILE I/O] Writing Mohawk-T to file: " + mohawkTFilename);
                Files.write(Paths.get(mohawkTFilename), mStr.getBytes());
            } catch (IOException e) {
                logger.warning("[FILE I/O] FAILED to write Mohawk-T to file: " + mohawkTFilename);
            }
        }

        // Convert Policy to NuSMV
        ConvertToNuSMV toNuSMV = new ConvertToNuSMV(timing, _timerKey);
        toNuSMV.convert(policy, _smv_File, true);

        if (skipRunning) {
            // Convert only
            result = ExecutionResult.CONVERT_ONLY;
        } else {
            // Run the converted file through the model checker
            _nusmv = new NuSMV(_programPath);

            switch (_mode) {
                case BMC :
                    Double bound = calculateDiameter(policy);

                    if (bound == -1) {
                        logger.severe("An error occured with calculating the Bound!");
                        result = ExecutionResult.ERROR_OCCURRED;
                    } else {
                        try {
                            if (_nusmv.BMCFile(bound.intValue(), toNuSMV.convertedFile.getAbsolutePath())) {
                                result = ExecutionResult.GOAL_REACHABLE;
                            } else {
                                result = ExecutionResult.GOAL_UNREACHABLE;
                            }
                        } catch (Exception e) {
                            logger.severe("An error occured when running: " + _programPath);
                            logger.severe(e.getMessage());
                            StringWriter errors = new StringWriter();
                            e.printStackTrace(new PrintWriter(errors));
                            result = ExecutionResult.ERROR_OCCURRED;
                        }
                    }

                    break;
                case SMC :
                    try {
                        if (_nusmv.SMCFile(toNuSMV.convertedFile.getAbsolutePath())) {
                            result = ExecutionResult.GOAL_REACHABLE;
                        } else {
                            result = ExecutionResult.GOAL_UNREACHABLE;
                        }
                    } catch (Exception e) {
                        logger.severe("An error occured when running: " + _programPath);
                        logger.severe(e.getMessage());
                        StringWriter errors = new StringWriter();
                        e.printStackTrace(new PrintWriter(errors));
                        result = ExecutionResult.ERROR_OCCURRED;
                    }
                    break;
                default :
                    logger.severe("No Option available to run in mode=" + _mode);
                    result = ExecutionResult.ERROR_OCCURRED;
                    break;
            }
        }

        return result;
    }

    /** Performs abstraction-refinement loop process till 1 of these results is found:
     * <ul>
     * <li><b>Goal Reached</b> - this means that the original policy is reachable
     * <li><b>No Refinement Steps Left</b> - this means that there are no more refinements available and that the result
     * is Goal Unreachable
     * <li><b>Error</b> - some error occurred
     * </ul>
     * 
     * @return */
    private ExecutionResult abstractionRefinementLoop() {
        ExecutionResult result = null;

        String errStr = "";

        AbstractionRefinment absref = new AbstractionRefinment(_testcase);

        File backupSMVFile = _smv_File;
        String baseFilename = _smv_File.getAbsolutePath();

        if (baseFilename.endsWith(".smv")) {
            baseFilename = baseFilename.substring(0, baseFilename.length() - 4);
        }

        while (true) {
            // Created a new policy
            errStr = absref.calculateNextPolicy();

            if (errStr.isEmpty()) {  // New policy successfully created
                if (debug) {
                    _smv_File = new File(
                            baseFilename + ".ABSREF_" + String.format("%02d", absref.getRefinementStep()) + ".smv");
                }

                ExecutionResult intermediateResult = convertAndRunModelChecker(absref.getRefinedPolicy(), _skipRunning);

                if (intermediateResult == ExecutionResult.GOAL_REACHABLE) {
                    result = ExecutionResult.GOAL_REACHABLE;

                    break; // break out of infinite loop
                } else if (intermediateResult == ExecutionResult.GOAL_UNREACHABLE) {
                    // Continue with the abstraction-refinement steps
                    continue;
                } else {
                    logger.severe("Error occurred when solving refined policy: " + intermediateResult);
                    result = intermediateResult;

                    break; // break out of infinite loop
                }
            } else { // failed to create a new policy
                switch (errStr) {
                    case "No Refinements Found\n" :
                        result = ExecutionResult.GOAL_UNREACHABLE;
                        break;
                    default :
                        logger.severe("Error occurred in Abstraction Refinement Step: " + errStr);
                        result = ExecutionResult.ERROR_OCCURRED;
                        break;
                }

                break; // break out of infinite loop
            }
        }

        _smv_File = backupSMVFile;

        return result;
    }

    /** A simple function to get the calculated diameter
     * 
     * @param testcase
     * @return */
    public Double calculateDiameter(MohawkT testcase) {
        BoundEstimation bound = new BoundEstimation(testcase, _policyFile.getAbsolutePath(), timing, _timerKey);
        bound.DEBUG = debug;
        return bound.calculateBound();
    }
}
