package mohawk.T.helper;

import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import mohawk.converter.to.nusmv.ConvertToNuSMV;
import mohawk.global.helper.FileHelper;
import mohawk.global.optimization.heuristics.MohawkTHeuristics;
import mohawk.global.pieces.MohawkT;
import mohawk.global.pieces.mohawk.NuSMVMode;
import mohawk.global.results.*;
import mohawk.global.timing.MohawkTiming;

/** @author Jonathan Shahen */
public class MohawkTTestingSuite {
    public final static Logger logger = Logger.getLogger("mohawk");

    public MohawkResults results;
    public MohawkTiming timing;
    public ExecutionResult lastResult;

    public String testingResultsFile = "logs/mohawkTestingResults.csv";
    /** The file-path for the SMV file output. <br/>
     * <b>NOTE:</b> The extension ".smv" will be added to this file. */
    public String smvFilepath = "logs/latestSMVFile";
    public String programPath;

    public NuSMVMode mode = NuSMVMode.SMC;

    public long TIMEOUT_SECONDS = 0;

    /** Flag to turn on DEBUGGING mode ON or OFF. <br/>
     * When ON extra output files will be generated to show more information about the steps that
     * were taken to come to the final result. */
    public boolean debug = false;

    public MohawkTTestingSuite(FileHelper fileHelper, MohawkResults results, MohawkTiming timing, String programPath) {
        this.results = results;
        this.timing = timing;
        this.programPath = programPath;
    }

    /** If you only want to convert SPEC files to SMV files. This can be used before running if there happens to be a
     * crash while running the tests you can just resume from the test and not have to convert the SPEC to SMV every
     * time.
     * 
     * @param m */
    public void convertToSMVFormat(MohawkT m) {
        logger.entering(getClass().getName(), "convertToSMVFormat()");

        ConvertToNuSMV convert = new ConvertToNuSMV(timing);

        convert.convert(m, new File(smvFilepath), true);

        if (convert.lastError != null) {
            logger.severe("Unable to convert Mohawk+T input policy to NuSMV!\nMohawk+T Input policy:" + m);
        }

        logger.exiting(getClass().getName(), "convertToSMVFormat()");
    }

    /** This will take care of fully running the test, if it needs to convert SPEC files it will and while it runs it
     * will write out results after each SMV file is run (just in case of a crash).
     * 
     * @param testcase Input Policy to test and return an result
     * @param origTimerName a way to create unique timer names within this function (because it is assumed to be called
     *            with more than once with different policies)
     * @param filename The base filename that the SMV file will be written to, this will then be sent to the model
     *            checker
     * @param writeOutResult When TRUE, the function will write and print out the results at the end of the function;
     *            when FALSE it will only print
     * @param abstractionRefinement When TRUE, the test will perform abstraction refinement on {@code testcase};
     *            otherwise the policy {@code testcase} will be converted to SMV and run in the model checker.
     * @param skipRunning When TRUE, the test will convert but not run the model checker
     * @return
     * @throws IOException */
    public ExecutionResult runTest(MohawkT testcase, String origTimerName, String filename, boolean writeOutResult,
            boolean abstractionRefinement, boolean skipRunning) throws IOException {
        logger.entering(getClass().getName(), "runTest()");

        FileWriter resultsFW = null;
        if (writeOutResult) {
            resultsFW = results.getFileWriter(new File(testingResultsFile), false);
            if (resultsFW == null) {
                logger.severe("[runTests] Unable to Create, or Write in, the results file: " + testingResultsFile);
                throw new IOException("Unable to Create, or Write in, the results file: " + testingResultsFile);
            }
        }

        ArrayList<NuSMVMode> modes = new ArrayList<NuSMVMode>();
        switch (mode) {
            case BOTH :// Both
                modes.add(NuSMVMode.SMC);
                modes.add(NuSMVMode.BMC);
                break;
            case BMC :// BMC
                modes.add(NuSMVMode.BMC);
                break;
            case SMC :// SMC
                modes.add(NuSMVMode.SMC);
                break;
            default :
                logger.severe("[ERROR] Unknown mode = " + mode);
        }

        ExecutorService executor = Executors.newSingleThreadExecutor();

        // convert to SMV and save in file
        convertToSMVFormat(testcase);
        File smvFile = new File(smvFilepath);

        for (Integer j = 0; j < modes.size(); j++) {
            /* TIMING */String timerName = origTimerName + "(Mode=" + modes.get(j) + ")";
            /* TIMING */timing.startTimer(timerName);

            logger.info("Working on " + timerName);

            /////////////////////////////////////////////////////////////////////
            // START HEURISTICS
            /* TIMING */timing.startTimer(timerName + "-heuristics");
            MohawkTHeuristics heuristics = new MohawkTHeuristics(testcase);
            heuristics.debug = debug;
            ExecutionResult result = heuristics.getResult();
            if (result != null) {
                lastResult = result;
                /* TIMING */timing.stopTimer(timerName + "-heuristics");
                /* TIMING */timing.stopTimer(timerName);

                // Store and print out the results
                results.add(new TestingResult(lastResult, // The ExecutionResult
                        timing.getLastElapsedTime(), // Duration
                        filename, // File of the Input Policy
                        modes.get(j).toString(), // SMC or BMC mode
                        "Not Setup Yet!",// Return Code from
                        "Heuristics Made Decision; Mode: " + modes.get(j)// Comment
                ));
                if (writeOutResult) {
                    results.writeOutLast(resultsFW);
                }
                continue;
            }
            /* TIMING */timing.stopTimer(timerName + "-heuristics");
            // END HEURISTICS
            /////////////////////////////////////////////////////////////////////

            // Test runner
            MohawkTTestRunner testRunner = new MohawkTTestRunner(testcase, programPath, filename, smvFile, modes.get(j),
                    timerName, timing, abstractionRefinement, skipRunning);

            testRunner.debug = debug;

            // Setup Timeout Timer
            Future<ExecutionResult> future = executor.submit(testRunner);
            try {
                logger.info("[RUNNING] Running with Mode: " + modes.get(j));

                if (TIMEOUT_SECONDS == 0) {
                    lastResult = future.get();
                } else {
                    lastResult = future.get(TIMEOUT_SECONDS, TimeUnit.SECONDS);
                }
                /* TIMING */timing.stopTimer(timerName);

                // Store and print out the results
                if (lastResult == null) {
                    logger.severe("ERROR: MohawkTTestRunner returned NULL instead of a ExectuionResult");
                    lastResult = ExecutionResult.ERROR_OCCURRED;
                    results.add(new TestingResult(lastResult, timing.getLastElapsedTime(), filename,
                            modes.get(j).toString(), "Not Setup Yet!",
                            "ERROR: MohawkTTestRunner returned NULL instead of a ExectuionResult"));
                } else {
                    results.add(new TestingResult(lastResult, timing.getLastElapsedTime(), filename,
                            modes.get(j).toString(), "Not Setup Yet!", ""));
                }

                if (writeOutResult) {
                    results.writeOutLast(resultsFW);
                }
            } catch (TimeoutException e) {
                logger.warning("[TIMEOUT] Mohawk has timed out for SPEC file: " + filename);

                lastResult = ExecutionResult.TIMEOUT;
                testRunner.kill();

                /* TIMING */timing.cancelTimer(timerName);
            } catch (OutOfMemoryError e) {
                logger.warning("[OUT OF MEMORY] Mohawk has ran out of memory out for SPEC file: " + filename);

                lastResult = ExecutionResult.OUT_OF_MEMORY;

                /* TIMING */timing.cancelTimer(timerName);
            } catch (InterruptedException e) {
                if (logger.getLevel() == Level.FINEST) {
                    e.printStackTrace();
                }
                /* TIMING */timing.cancelTimer(timerName);
                logger.severe(e.getMessage());
            } catch (ExecutionException e) {
                StringWriter errors = new StringWriter();
                e.printStackTrace(new PrintWriter(errors));
                logger.severe(errors.toString());
                logger.severe(e.getMessage());
                /* TIMING */timing.cancelTimer(timerName);
            }

            logger.info("[COMPLETED] Result (Mode: " + modes.get(j) + "): " + lastResult + ", Elapsed Time: "
                    + timing.getLastElapsedTimeSec() + " secs; for the Query: '" + testcase.query + "'");
        }
        executor.shutdown();

        if (writeOutResult) {
            resultsFW.close();
        }

        logger.exiting(getClass().getName(), "runTest()");
        return lastResult;
    }
}
