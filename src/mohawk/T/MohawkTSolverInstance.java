package mohawk.T;

import java.io.*;
import java.security.InvalidParameterException;
import java.util.logging.*;

import org.apache.commons.cli.*;
import org.apache.commons.lang3.StringUtils;

import mohawk.T.helper.MohawkTTestingSuite;
import mohawk.global.formatter.MohawkCSVFileFormatter;
import mohawk.global.formatter.MohawkConsoleFormatter;
import mohawk.global.helper.FileHelper;
import mohawk.global.helper.ParserHelper;
import mohawk.global.optimization.boundestimation.BoundEstimation;
import mohawk.global.optimization.slicing.StaticSlicer;
import mohawk.global.parser.mohawkT.MohawkTARBACParser;
import mohawk.global.pieces.MohawkT;
import mohawk.global.pieces.mohawk.NuSMVMode;
import mohawk.global.results.*;
import mohawk.global.timing.MohawkTiming;

public class MohawkTSolverInstance {
    private final String VERSION = "v1.0.0";
    private static final String AUTHORS = "Jonathan Shahen <jmshahen [AT] uwaterloo [DOT] ca>";

    // Logger Fields
    public static final Logger logger = Logger.getLogger("mohawk");
    private String Logger_filepath = "logs/MohawkT-Log.csv";
    private ConsoleHandler consoleHandler = new ConsoleHandler();
    private Level LoggerLevel;
    private FileHandler fileHandler;
    private Boolean WriteCSVFileHeader = true;
    private String resultsFile = "logs/MohawkT-Results.csv";
    private String diameterResultsFile = "logs/MohawkT-Diameter-Results.csv";
    private String slicerResultsFile = "logs/MohawkT-StaticSlicer-Results.csv";
    private String timingFile = "logs/MohawkT-Timing.csv";

    // Reference
    private String NuSMV_filepath = "programs/NuSMV-2.6.0-win64/bin/NuSMV.exe";
    private String NuSMVZChaff_filepath = "programs/NuSMV-ZCHAFF-2.6.0-win64/bin/NuSMV.exe";
    private String NuXMV_filepath = "programs/nuXmv-1.1.1-win64/bin/nuXmv.exe";
    private String programPath = NuSMV_filepath;

    // Helpers
    public MohawkResults results;
    public DiameterResults diameterResults;
    public StaticSlicerResults slicerResults;
    public MohawkTiming timing;
    public MohawkTTestingSuite tests;
    public FileHelper fileHelper = new FileHelper();
    public ParserHelper parserHelper = new ParserHelper();
    // private SMVSpecHelper SMV_helper = new SMVSpecHelper();

    // Settings
    private long TIMEOUT_SECONDS = 0;
    private boolean abstractionRefinement = true;
    private boolean skipRunning = false;
    /** Flag to turn on or off debug settings. When equal to TRUE, more files are produced:
     * <ul>
     * <li>Each step of Abstraction-Refinement is kept (both Mohawk+T and SMV file)
     * <li>Each step of the Bound Estimation is recorded
     * <li>Each Heuristic is recorded
     * <li><b>This does not change the logging level: console output does not change</b>
     * </ul>
    */
    public boolean debug = false;

    /* ********* FUNCTIONS ********* */
    public Level getLoggerLevel() {
        return LoggerLevel;
    }

    public MohawkTTestingSuite getTestingSuite() {
        return tests;
    }

    public void setLoggerLevel(Level loggerLevel) {
        LoggerLevel = loggerLevel;
    }

    public void printHelp(CommandLine cmd, Options options) throws NumberFormatException {
        if (cmd.hasOption(MohawkTSolverOptionString.MAXW.toString())) {
            try {
                Integer maxw = Integer.decode(cmd.getOptionValue(MohawkTSolverOptionString.MAXW.toString()));
                printHelp(options, maxw);
            } catch (NumberFormatException e) {
                printHelp(options, 80);

                e.printStackTrace();
                throw new NumberFormatException("An error occured when trying to print out the help options!");
            }
        } else {
            printHelp(options, 80);
        }
    }

    public void printHelp(Options options, int maxw) {
        HelpFormatter f = new HelpFormatter();
        f.printHelp(maxw, "mohawk",
                StringUtils.repeat("-", maxw) + "\nAuthors: " + AUTHORS + "\n" + StringUtils.repeat("-", 20), options,
                StringUtils.repeat("-", maxw), true);
    }

    public CommandLine init(String[] args) throws ParseException, SecurityException, IOException, Exception {
        Options options = new Options();
        setupOptions(options);
        CommandLineParser parser = new BasicParser();
        CommandLine cmd = parser.parse(options, args);

        setupLoggerOptions(cmd, options);
        if (setupReturnImmediatelyOptions(cmd, options)) { return null; }

        setupUserPreferenceOptions(cmd, options);
        setupFileOptions(cmd, options);
        setupResultOptions(cmd, options);
        setupControlOptions(cmd, options);

        return cmd;
    }

    public int run(String[] args) {
        try {
            timing = new MohawkTiming();
            results = new MohawkResults();
            diameterResults = new DiameterResults();
            slicerResults = new StaticSlicerResults();
            tests = new MohawkTTestingSuite(fileHelper, results, timing, programPath);

            ////////////////////////////////////////////////////////////////////////////////
            // COMMANDLINE OPTIONS
            CommandLine cmd = init(args);
            if (cmd == null) { return 0; }

            // Only set tests.debug when equal to TRUE (this allows for default to be changed to TRUE)
            if (debug) {
                tests.debug = true;
            }
            ////////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////////
            // LOADING ALL FILES
            /* Timing */timing.startTimer("loadFile");
            fileHelper.loadSpecFiles();
            /* Timing */timing.stopTimer("loadFile");
            ////////////////////////////////////////////////////////////////////////////////

            // Execute the test cases
            if (cmd.hasOption(MohawkTSolverOptionString.RUN.toString())) {
                logger.info("[ACTION] Run paramter detected");

                tests.testingResultsFile = resultsFile;
                tests.TIMEOUT_SECONDS = TIMEOUT_SECONDS;

                // Diameter Results
                FileWriter diameterFW = diameterResults.getFileWriter(new File(diameterResultsFile), false);
                // Static Slicing Results
                FileWriter slicerFW = slicerResults.getFileWriter(new File(slicerResultsFile), false);

                String runVal = cmd.getOptionValue(MohawkTSolverOptionString.RUN.toString());
                Integer numFiles = fileHelper.specFiles.size();

                ////////////////////////////////////////////////////////////////////////////////
                // INDIVIDUAL FILE LOOP
                logger.info("Mohawk+T Policy Files to Solve: " + fileHelper.printFileNames(300, true));
                for (Integer i = 1; i <= fileHelper.specFiles.size(); i++) {
                    ////////////////////////////////////////////////////////////////////////////////
                    /* TIMING */String timerName = "mainLoop (" + i + "/" + numFiles + ")";
                    /* TIMING */timing.startTimer(timerName);
                    File specFile = fileHelper.specFiles.get(i - 1);
                    ////////////////////////////////////////////////////////////////////////////////

                    ////////////////////////////////////////////////////////////////////////////////
                    // PARSING MOHAWK+T FILE
                    /* Timing */timing.startTimer(timerName + "-parseFile (" + i + ")");
                    logger.info("Processing File (" + i + "/" + numFiles + "): " + specFile.getAbsolutePath());
                    MohawkTARBACParser parser = parserHelper.parseMohawkTFile(specFile);
                    /* Timing */timing.stopTimer(timerName + "-parseFile (" + i + ")");

                    MohawkT m = parser.mohawkT;

                    if (parserHelper.error.errorFound) {
                        logger.warning("[PARSING] ERROR: Skipping this file due to a parsing error");
                        continue;
                    } else {
                        logger.info("[PARSING] No errors found while parsing file, continuing on to converting");
                    }
                    // END OF PARSING MOHAWK+T FILE
                    ////////////////////////////////////////////////////////////////////////////////

                    ////////////////////////////////////////////////////////////////////////////////
                    // RUN A CERTAIN TASK ON THE MOHAWK+T FILE
                    switch (runVal) {
                        case "all" :
                            logger.info("[ALL ACTION] Will convert and then execute the testcase provided");
                            tests.runTest(m, timerName, specFile.getAbsolutePath(), true, abstractionRefinement,
                                    skipRunning);
                            break;
                        case "smv" :
                            logger.info("[SMV ACTION] Will only convert the testcase provided");
                            tests.convertToSMVFormat(m);
                            break;
                        case "dia" :
                        case "diameter" : {
                            logger.info("[DIAMETER ACTION] Will only calculate the estimated diameter "
                                    + "of the testcase provided");
                            BoundEstimation bound = new BoundEstimation(m, specFile.getAbsolutePath(), timing,
                                    timerName);
                            bound.DEBUG = debug;
                            Double diameter = bound.calculateBound();

                            /* Timing */timing.startTimer(timerName + "-writeOutDiameterResults (" + i + ")");
                            diameterResults.add(bound.diaResult);
                            diameterResults.writeOutLast(diameterFW);
                            /* Timing */timing.stopTimer(timerName + "-writeOutDiameterResults (" + i + ")");

                            logger.info("[DIAMETER] The calculated diameter is: " + diameter);
                            System.out.println("[DIAMETER] The calculated diameter is: " + diameter);
                        }
                            break;
                        case "slice" : {
                            logger.info("[Static Slicing ACTION] Will only perform static slicing on "
                                    + "the input policies.");
                            StaticSlicer slicer = new StaticSlicer(m, specFile.getAbsolutePath(), timing, timerName);
                            slicer.DEBUG = debug;
                            slicer.writeOutNewPolicy = true;
                            slicer.newPolicyFilename = slicer.origPolicyFilename + ".sliced.mohawkT";
                            slicer.slicePolicy();

                            /* Timing */timing.startTimer(timerName + "-writeOutSlicerResults (" + i + ")");
                            slicerResults.add(slicer.slicerResult);
                            slicerResults.writeOutLast(slicerFW);
                            /* Timing */timing.stopTimer(timerName + "-writeOutSlicerResults (" + i + ")");

                            logger.info("[SLICING] " + slicer.slicerResult);
                            System.out.println("[SLICING] " + slicer.slicerResult);
                        }
                            break;
                        default :
                            logger.severe("The Run Option '" + runVal + "' has not been implemented. "
                                    + "Please see use 'mohawk -help' to see which Run Options have been implemented");
                            /* TIMING */timing.cancelTimer(timerName);
                            continue;
                    }
                    // END OF RUN A CERTAIN TASK ON THE MOHAWK+T FILE
                    ////////////////////////////////////////////////////////////////////////////////

                    /* TIMING */timing.stopTimer(timerName);
                }
                // END OF INDIVIDUAL FILE LOOP
                ////////////////////////////////////////////////////////////////////////////////

                ////////////////////////////////////////////////////////////////////////////////
                // CLEAN UP FILE HANDLES
                diameterFW.close();
                slicerFW.close();
                ////////////////////////////////////////////////////////////////////////////////
            }

            logger.info("[EOF] MohawkTSolver is done running");
            System.out.println("[EOF] MohawkTSolver is done running");

            logger.info("[TIMING] " + tests.timing);
            timing.writeOut(new File(timingFile), false);
        } catch (ParseException e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            logger.severe(errors.toString());
            logger.severe(e.getMessage());

            return -2;
        } catch (Exception e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            logger.severe(errors.toString());
            logger.severe(e.getMessage());

            return -1;
        } finally {
            for (Handler h : logger.getHandlers()) {
                h.close();// must call h.close or a .LCK file will remain.
            }
        }
        return 0;
    }

    /** Adds all of the available options to input parameter.
     * 
     * @param options */
    @SuppressWarnings("static-access")
    public void setupOptions(Options options) {
        /** If someone is trying to just display the help menu, we need {@link MohawkTSolverInstance#tests}
         * to be initialized */
        if (tests == null) {
            tests = new MohawkTTestingSuite(new FileHelper(), new MohawkResults(), new MohawkTiming(), programPath);
        }

        // Add Information Options
        options.addOption(MohawkTSolverOptionString.HELP.toString(), false, "Print this message");
        options.addOption(MohawkTSolverOptionString.VERSION.toString(), false,
                "Prints the version (" + VERSION + ") information");
        options.addOption(MohawkTSolverOptionString.CHECKNUSMV.toString(), false,
                "Checks that NuSMV is on the system and displays which version is installed");

        // Add Logging Level Options
        options.addOption(OptionBuilder.withArgName("quiet|debug|verbose")
                .withDescription("Be extra quiet only errors are shown; " + "Show debugging information; "
                        + "extra information is given for Verbose; " + "default is warning level")
                .hasArg().create(MohawkTSolverOptionString.LOGLEVEL.toString()));

        options.addOption(OptionBuilder.withArgName("logfile|'n'|'u'")
                .withDescription("The filepath where the log file should be created; "
                        + "No file will be created when equal to 'n'; "
                        + "A unique filename will be created when equal to 'u'; " + "default it creates a log called '"
                        + Logger_filepath + "'")
                .hasArg().create(MohawkTSolverOptionString.LOGFILE.toString()));

        options.addOption(MohawkTSolverOptionString.NOHEADER.toString(), false,
                "Does not write the CSV file header to the output log");

        options.addOption(
                OptionBuilder.withArgName("csvfile").withDescription("The file where the result should be stored")
                        .hasArg().create(MohawkTSolverOptionString.RESULTSFILE.toString()));

        // custom Console Logging Options
        options.addOption(
                OptionBuilder.withArgName("num").withDescription("The maximum width of the console (default 120)")
                        .hasArg().create(MohawkTSolverOptionString.MAXW.toString()));

        options.addOption(OptionBuilder.withArgName("string")
                .withDescription("The new line string when wrapping a long line (default '\\n    ')").hasArg()
                .create(MohawkTSolverOptionString.LINESTR.toString()));

        options.addOption(MohawkTSolverOptionString.DEBUG.toString(), false,
                "Add this flag to get more ouput about the steps that are taken by Mohawk+T "
                        + "(drastically slows down performance)");

        // Add File IO Options
        options.addOption(OptionBuilder.withArgName("file|folder")
                .withDescription("Path to the RBAC Spec file or Folder if the 'bulk' option is set").hasArg()
                .create(MohawkTSolverOptionString.SPECFILE.toString()));

        options.addOption(OptionBuilder.withArgName("nusmv|znusmv|nuxmv")
                .withDescription("Which version of NuSMV to use: NuSMV (default), NuSMV with ZChaff, or NuXMV").hasArg()
                .create(MohawkTSolverOptionString.PROGRAM.toString()));

        options.addOption(OptionBuilder.withArgName("smvfile|'n'")
                .withDescription("The file/folder path where the SMV file(s) should be created; "
                        + "Only temporary files will be used when equal to 'n'; "
                        + "by default it creates a SMV called '" + tests.smvFilepath + "'")
                .hasArg().create(MohawkTSolverOptionString.SMVFILE.toString()));

        options.addOption(OptionBuilder.withArgName("extension")
                .withDescription(
                        "File extention used when searching for SPEC files when the 'bulk' option is used. Default:'"
                                + fileHelper.fileExt + "'")
                .hasArg().create(MohawkTSolverOptionString.SPECEXT.toString()));

        ///////////////////////////////////////////////////////////////////////////////////////////
        // Add Functional Options
        options.addOption(MohawkTSolverOptionString.BULK.toString(), false,
                "Use the folder that rbacspec points to and run against all *.spec");

        options.addOption(OptionBuilder.withArgName("bmc|smc|both")
                .withDescription("Uses the Bound Estimation Checker when equal to 'bmc'; "
                        + "Uses Symbolic Model Checking when equal to 'smc';" + "Default is: " + tests.mode)
                .hasArg().create(MohawkTSolverOptionString.MODE.toString()));

        options.addOption(OptionBuilder.withArgName("seconds")
                .withDescription(
                        "The timeout time in seconds for Mohawk's refinement loop. Default: " + TIMEOUT_SECONDS)
                .hasArg().create(MohawkTSolverOptionString.TIMEOUT.toString()));
        ///////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////
        // Control Flags
        options.addOption(MohawkTSolverOptionString.ABSTRACTION_REFINEMENT.toString(), false,
                "Perform abstraction refinement on the selected testcases");

        options.addOption(MohawkTSolverOptionString.SKIP_RUNNING.toString(), false,
                "Used to skip running the model checker and only convert to SMV format");
        ///////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////
        // Add Actionable Options
        options.addOption(OptionBuilder.withArgName("all|smv|diameter")
                .withDescription("Runs the whole model checker when equal to 'all'; "
                        + "Runs only the SMV conversion when equal to 'smv'; "
                        + "Calculates the diameter when equal to 'diameter'")
                .hasArg().create(MohawkTSolverOptionString.RUN.toString()));
        ///////////////////////////////////////////////////////////////////////////////////////////
    }

    /** Sets up the options that do not process anything in detail, but returns a specific informative result.<br/>
     * Examples include:
     * <ul>
     * <li>Help
     * <li>Version
     * <li>Check NuSMV programs
     * </ul>
     * 
     * @param cmd
     * @param options
     * @return
     * @throws NumberFormatException */
    private Boolean setupReturnImmediatelyOptions(CommandLine cmd, Options options) throws NumberFormatException {
        if (cmd.hasOption(MohawkTSolverOptionString.HELP.toString()) == true || cmd.getOptions().length < 1) {
            printHelp(cmd, options);
            return true;
        }

        if (cmd.hasOption(MohawkTSolverOptionString.VERSION.toString())) {
            // keep it as simple as possible for the version
            System.out.println(VERSION);
            return true;
        }

        if (cmd.hasOption(MohawkTSolverOptionString.CHECKNUSMV.toString())) {
            try {
                File path = new File(NuSMV_filepath);

                if (!path.exists()) {
                    logger.warning("[NuSMV] The NuSMV filepath points to no file: " + NuSMV_filepath);
                } else {
                    String[] commands = {path.getAbsolutePath(), "-help"};
                    ProcessBuilder pb = new ProcessBuilder(commands);
                    pb.redirectErrorStream(true);// REQUIRED: NuSVM uses STDERR
                    // will throw error if it cannot find NuSMV
                    Process proc = pb.start();

                    BufferedReader br = new BufferedReader(new InputStreamReader(proc.getInputStream()));

                    String strLine = null;
                    boolean noOutput = true;
                    while ((strLine = br.readLine()) != null) {
                        noOutput = false;

                        if (strLine.contains("This is NuSMV")) {
                            logger.info("[NuSMV] " + strLine);
                            break;
                        }
                    }
                    if (noOutput) {
                        logger.warning(
                                "[NuSMV] No output was given by NuSMV, maybe the commandline arguments have changed "
                                        + "or the file " + NuSMV_filepath + " points to the wrong file");
                    }
                }
            } catch (IOException e) {
                logger.severe("[NuSMV] No Version of NuSMV was found, please check the file path: " + NuSMV_filepath);
            }

            try {
                File path = new File(NuSMVZChaff_filepath);

                if (!path.exists()) {
                    logger.warning("[NuSMV with ZCHAFF] The NuSMV with ZCHAFF filepath points to no file: "
                            + NuSMVZChaff_filepath);
                } else {
                    String[] commands = {path.getAbsolutePath(), "-help"};
                    ProcessBuilder pb = new ProcessBuilder(commands);
                    pb.redirectErrorStream(true);// REQUIRED: NuSVM uses STDERR
                    // will throw error if it cannot find NuSMV
                    Process proc = pb.start();

                    BufferedReader br = new BufferedReader(new InputStreamReader(proc.getInputStream()));

                    String strLine = null;
                    boolean noOutput = true;
                    while ((strLine = br.readLine()) != null) {
                        noOutput = false;
                        if (strLine.contains("This is NuSMV")) {
                            logger.info("[NuSMV with ZCHAFF] " + strLine);
                            break;
                        }
                    }
                    if (noOutput) {
                        logger.warning("[NuSMV with ZCHAFF] No output was given by NuSMV with ZCHAFF, "
                                + "maybe the commandline arguments have changed " + "or the file "
                                + NuSMVZChaff_filepath + " points to the wrong file");
                    }
                }
            } catch (IOException e) {
                logger.severe("[NuSMV with ZCHAFF] No Version of NuSMV with ZCHAFF was found, "
                        + "please check the file path: " + NuSMVZChaff_filepath);
            }

            try {
                File path = new File(NuXMV_filepath);

                if (!path.exists()) {
                    logger.warning("[NuXMV] The NuXMV filepath points to no file: " + NuXMV_filepath);
                } else {
                    String[] commands = {path.getAbsolutePath(), "-help"};
                    ProcessBuilder pb = new ProcessBuilder(commands);
                    pb.redirectErrorStream(true);// REQUIRED: NuSVM uses STDERR
                    // will throw error if it cannot find NuSMV
                    Process proc = pb.start();

                    BufferedReader br = new BufferedReader(new InputStreamReader(proc.getInputStream()));

                    String strLine = null;
                    boolean noOutput = true;
                    while ((strLine = br.readLine()) != null) {
                        noOutput = false;
                        if (strLine.contains("This is nuXmv")) {
                            logger.info("[NuXMV] " + strLine);
                            break;
                        }
                    }
                    if (noOutput) {
                        logger.warning("[NuXMV] No output was given by NuXMV, maybe the commandline arguments "
                                + "have changed or the file " + NuXMV_filepath + " points to the wrong file");
                    }
                }
            } catch (IOException e) {
                logger.severe("[NuXMV] No Version of NuXMV was found, please check that file path: " + NuXMV_filepath);
            }
            return true;
        }

        return false;
    }

    private void setupLoggerOptions(CommandLine cmd, Options options) throws SecurityException, IOException {
        // Logging Level
        logger.setUseParentHandlers(false);
        consoleHandler.setFormatter(new MohawkConsoleFormatter());
        setLoggerLevel(Level.FINEST);// Default Level
        if (cmd.hasOption(MohawkTSolverOptionString.LOGLEVEL.toString())) {
            String loglevel = cmd.getOptionValue(MohawkTSolverOptionString.LOGLEVEL.toString());
            if (loglevel.equalsIgnoreCase("quiet")) {
                setLoggerLevel(Level.WARNING);
            } else if (loglevel.equalsIgnoreCase("debug")) {
                setLoggerLevel(Level.FINEST);
            } else if (loglevel.equalsIgnoreCase("verbose")) {
                setLoggerLevel(Level.INFO);
            }
        }

        logger.setLevel(LoggerLevel);
        consoleHandler.setLevel(LoggerLevel);
        logger.addHandler(consoleHandler);

        // Add CSV File Headers
        if (cmd.hasOption(MohawkTSolverOptionString.NOHEADER.toString())) {
            WriteCSVFileHeader = false;
        }

        // Set File Logger
        if (cmd.hasOption(MohawkTSolverOptionString.LOGFILE.toString())) {
            // Check if no log file was requested
            if (cmd.getOptionValue(MohawkTSolverOptionString.LOGFILE.toString()).equals("n")) {
                // Create no log file
                Logger_filepath = "";
            } else if (cmd.getOptionValue(MohawkTSolverOptionString.LOGFILE.toString()).equals("u")) {
                // Create a unique log file
                Logger_filepath = "mohawk-log.%u.%g.txt";
            } else {
                try {
                    // Create a log file with a specific name
                    File logfile = new File(cmd.getOptionValue(MohawkTSolverOptionString.LOGFILE.toString()));

                    if (!logfile.exists()) {
                        logfile.createNewFile();
                    }
                    Logger_filepath = logfile.getAbsolutePath();

                    if (WriteCSVFileHeader) {
                        FileOutputStream writer = new FileOutputStream(logfile, true);// Always append!
                        writer.write(MohawkCSVFileFormatter.csvHeaders().getBytes());
                        writer.flush();
                        writer.close();
                    }

                } catch (IOException e) {
                    logger.severe(e.getMessage());
                    return;
                }
            }
        } else {
            File logfile = new File(Logger_filepath);

            if (!logfile.exists()) {
                logfile.createNewFile();
                if (WriteCSVFileHeader) {
                    FileOutputStream writer = new FileOutputStream(logfile, true);// Always append!
                    writer.write(MohawkCSVFileFormatter.csvHeaders().getBytes());
                    writer.flush();
                    writer.close();
                }
            }
        }

        // Add Logger File Handler
        if (!Logger_filepath.isEmpty()) {
            File f = new File(Logger_filepath); // TODO: test this
            f.getParentFile().mkdirs();
            fileHandler = new FileHandler(Logger_filepath, true);
            fileHandler.setLevel(getLoggerLevel());
            fileHandler.setFormatter(new MohawkCSVFileFormatter());
            logger.addHandler(fileHandler);
        }

        if (cmd.hasOption(MohawkTSolverOptionString.DEBUG.toString())) {
            debug = true;
        } else {
            debug = false;
        }
        logger.info("[Option] Debug Mode: " + ((abstractionRefinement) ? "ENABLED" : "DISABLED"));
    }

    /** Looks for the following options and sets up the program accordingly:
     * <ul>
     * <li>{@link MohawkTSolverOptionString#ABSTRACTION_REFINEMENT}
     * <li>{@link MohawkTSolverOptionString#BOUND_ESTIMATION}
     * <li>{@link MohawkTSolverOptionString#PROGRAM}
     * </ul>
     * 
     * @param cmd
     * @param options */
    private void setupControlOptions(CommandLine cmd, Options options) {
        if (cmd.hasOption(MohawkTSolverOptionString.PROGRAM.toString())) {
            logger.fine("[OPTION] Changing the model checker program");
            switch (cmd.getOptionValue(MohawkTSolverOptionString.PROGRAM.toString())) {
                case "nusmv" :
                    programPath = NuSMV_filepath;
                    break;
                case "znusmv" :
                    programPath = NuSMVZChaff_filepath;
                    break;
                case "nuxmv" :
                    programPath = NuXMV_filepath;
                    break;
                default :
                    logger.warning("Unknown Model Checker Program:"
                            + cmd.getOptionValue(MohawkTSolverOptionString.PROGRAM.toString()));
            }
            logger.info("Using Model Checker: " + programPath);
        }

        if (cmd.hasOption(MohawkTSolverOptionString.ABSTRACTION_REFINEMENT.toString())) {
            abstractionRefinement = true;
        }
        logger.info("[Option] Abstraction Refinement: " + ((abstractionRefinement) ? "ENABLED" : "DISABLED"));

        if (cmd.hasOption(MohawkTSolverOptionString.SKIP_RUNNING.toString())) {
            logger.info("[Option] Skip Running: ENABLED");
            skipRunning = true;
        }
        logger.info("[Option] Skip Running Model Checker: " + ((skipRunning) ? "ENABLED" : "DISABLED"));

    }

    private void setupResultOptions(CommandLine cmd, Options options) {
        if (cmd.hasOption(MohawkTSolverOptionString.RESULTSFILE.toString())) {
            logger.fine("[OPTION] Changing the results file");
            resultsFile = cmd.getOptionValue(MohawkTSolverOptionString.RESULTSFILE.toString());
        }
        logger.info("Results File: " + resultsFile);
    }

    private void setupFileOptions(CommandLine cmd, Options options) {
        // Load in SPEC Files
        // SMV File
        if (cmd.hasOption(MohawkTSolverOptionString.SMVFILE.toString())) {
            if (cmd.getOptionValue(MohawkTSolverOptionString.SMVFILE.toString()).equals("n")) {
                logger.fine("[OPTION] Using temporary SMV Files - will be deleted after each use");
                fileHelper.smvDeleteFile = true;
            } else {
                logger.fine("[OPTION] Using a specific SMV File: "
                        + cmd.getOptionValue(MohawkTSolverOptionString.SMVFILE.toString()));
                fileHelper.smvFilepath = cmd.getOptionValue(MohawkTSolverOptionString.SMVFILE.toString());
            }
        } else {
            logger.fine("[OPTION] No SMV Filename included, saving file under: " + fileHelper.smvFilepath);
        }

        // Grab the SPEC file
        if (cmd.hasOption(MohawkTSolverOptionString.SPECFILE.toString())) {
            logger.fine("[OPTION] Using a specific SPEC File: "
                    + cmd.getOptionValue(MohawkTSolverOptionString.SPECFILE.toString()));
            fileHelper.specFile = cmd.getOptionValue(MohawkTSolverOptionString.SPECFILE.toString());
        } else {
            logger.fine("[OPTION] No Spec File included");
        }

        if (cmd.hasOption(MohawkTSolverOptionString.SPECEXT.toString())) {
            logger.fine("[OPTION] Using a specific SPEC File Extension: "
                    + cmd.getOptionValue(MohawkTSolverOptionString.SPECEXT.toString()));
            fileHelper.fileExt = cmd.getOptionValue(MohawkTSolverOptionString.SPECEXT.toString());
        } else {
            logger.fine("[OPTION] Using the default SPEC File Extension: " + fileHelper.fileExt);
        }

        // Load more than one file from the SPEC File?
        if (cmd.hasOption(MohawkTSolverOptionString.BULK.toString())) {
            logger.fine("[OPTION] Bulk SPEC File inclusion: Enabled");
            fileHelper.bulk = true;
        } else {
            logger.fine("[OPTION] Bulk SPEC File inclusion: Disabled");
            fileHelper.bulk = false;
        }

        // Set the Mode of Operation for Converting SPEC to SMV
        if (cmd.hasOption(MohawkTSolverOptionString.MODE.toString())) {
            switch (cmd.getOptionValue(MohawkTSolverOptionString.MODE.toString())) {
                case "bmc" :
                    tests.mode = NuSMVMode.BMC;
                    break;
                case "smc" :
                    tests.mode = NuSMVMode.SMC;
                    break;
                case "both" :
                    tests.mode = NuSMVMode.BOTH;
                    break;
                default :
                    throw new InvalidParameterException(
                            "[ERROR] Unknown mode: " + cmd.getOptionValue(MohawkTSolverOptionString.MODE.toString()));
            }
        }

        // Load more than one file from the SPEC File?
        if (cmd.hasOption(MohawkTSolverOptionString.TIMEOUT.toString())) {
            logger.fine("[OPTION] Timeout: " + cmd.getOptionValue(MohawkTSolverOptionString.TIMEOUT.toString())
                    + " seconds");
            TIMEOUT_SECONDS = new Long(cmd.getOptionValue(MohawkTSolverOptionString.TIMEOUT.toString()));
        } else {
            logger.fine("[OPTION] Timeout: No Timeout");
        }
    }

    private void setupUserPreferenceOptions(CommandLine cmd, Options options) {
        // Set the Console's Max Width
        if (cmd.hasOption(MohawkTSolverOptionString.MAXW.toString())) {
            logger.fine("[OPTION] Setting the console's maximum width");
            String maxw = "";
            try {
                maxw = cmd.getOptionValue(MohawkTSolverOptionString.MAXW.toString());
                ((MohawkConsoleFormatter) consoleHandler.getFormatter()).maxWidth = Integer.decode(maxw);
            } catch (NumberFormatException e) {
                logger.severe("[ERROR] Could not decode 'maxw': " + maxw + ";\n" + e.getMessage());
            }
        } else {
            logger.fine("[OPTION] Default Console Maximum Width Used");
        }

        // Set the Console's Wrap String
        if (cmd.hasOption(MohawkTSolverOptionString.LINESTR.toString())) {
            logger.fine("[OPTION] Setting the console's new line string");
            ((MohawkConsoleFormatter) consoleHandler.getFormatter()).newLineStr = cmd
                    .getOptionValue(MohawkTSolverOptionString.LINESTR.toString());
        } else {
            logger.fine("[OPTION] Default Line String Used");
        }

    }
}
