# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.0] - 2015-08-15
### Added
- Readme
- Changelog
- Version File
- All of the initial source code to read in and display Mohawk+T files
